from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
from flaskr import auth

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    db = get_db()
    users = db.execute(
        'SELECT *'
        'FROM user WHERE role = "employee"'
        'ORDER BY registered DESC'
    ).fetchall()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    print('from index render users: ',users)
    return render_template('blog/index.html', posts=posts, users=users)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')



def get_post(id, check_author=True):
    post = get_db().execute(
        ' SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post



@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('GET','POST'))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))


@bp.route('/profile', methods=('GET', 'POST'))
@login_required
def profile():
    if request.method == 'POST':
        # title = request.form['title']
        # body = request.form['body']
        # error = None

        # if not title:
        #     error = 'Title is required.'

        # if error is not None:
        #     flash(error)
        # else:
            
        #     db = get_db()
        #     db.execute(
        #         'INSERT INTO post (title, body, author_id)'
        #         ' VALUES (?, ?, ?)',
        #         (title, body, g.user['id'])
        #     )
        #     db.commit()
            return redirect(url_for('blog.index'))
   
   
    # print(g.user['registered'])
    return render_template('blog/profile.html')


@bp.route('/<int:id>/hire', methods=('GET', 'POST'))
@login_required
def hire(id):
    # get_user(id)
    # user_id = id

    # employer = g.user['id']
    # print(employer,id)
    db = get_db()
    db.execute(
        'UPDATE user SET hired_by = ?'
        'WHERE id  = ?', 
        (g.user['id'], id)
    )
    db.commit()
    return redirect(url_for('blog.index'))


@bp.route('/<int:id>/fire', methods=('GET', 'POST'))
@login_required
def fire(id):

    db = get_db()
    db.execute(
        'UPDATE user SET hired_by = ?'
        'WHERE id  = ?', 
        (0 , id)
    )
    db.commit()
    return redirect(url_for('blog.index'))

