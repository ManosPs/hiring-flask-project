# Server
    
    git clone https://gitlab.com/ManosPs/hiring-flask-project.git
    cd hiring-flask-project
    python3 -m venv venv
    source venv/bin/activate
    python3 -m pip install -r requirements.txt
    export FLASK_APP=flaskr
    export FLASK_RUN_HOST=0.0.0.0
    export FLASK_RUN_PORT=80
    flask init-db
    flask run


# Flaskr

The basic blog app built in the Flask [tutorial](https://flask.palletsprojects.com/tutorial).
## Install

Be sure to use the same version of the code as the version of the docs you're reading. You probably want the latest tagged version, but the default Git version is the main branch.

    # clone the repository
    $ git clone https://github.com/pallets/flask
    $ cd flask
    # checkout the correct version
    $ git tag  # shows the tagged versions
    $ git checkout latest-tag-found-above
    $ cd examples/tutorial`
Create a virtualenv and activate it:

    $ python3 -m venv venv
    $ . venv/bin/activate

Or on Windows cmd:

    $ py -3 -m venv venv
    $ venv\Scripts\activate.bat

Install Flaskr:

    $ pip install -e .

Or if you are using the main branch, install Flask from source before installing Flaskr:

    $ pip install -e ../..
    $ pip install -e .

Run

    $ export FLASK_APP=flaskr
    $ export FLASK_ENV=development
    $ flask init-db
    $ flask run

Or on Windows cmd:

    > set FLASK_APP=flaskr
    > set FLASK_ENV=development
    > flask init-db
    > flask run

Open http://127.0.0.1:5000 in a browser.
Test

    $ pip install '.[test]'
    $ pytest

Run with coverage report:

    $ coverage run -m pytest
    $ coverage report
    $ coverage html  # open htmlcov/index.html in a browser
